const fs = require("fs");
const {successResponse} = require("../helper/response");

class User {
	constructor() {
		this.data = JSON.parse(fs.readFileSync("./data/data.json"));
	}
	//Home Page
	home = (req, res) => {
		res.render("../views/index");
	};

	// Game Page
	game = (req, res) => {
		res.render("../views/game");
	};

	// Read user file
	getUser = (req, res) => {
		successResponse(res, 200, this.data);
	};
	// Create new user
	createUser = (req, res) => {
		let body = req.body;
		this.data.push(body);
		fs.writeFileSync("./data/data.json", JSON.stringify(this.data));
		successResponse(res, 201, this.data, {total: this.data.length});
	};
	// Update user files
	updateUser = (req, res) => {
		let index = req.params.index;
		let body = req.body;
		this.data[index].username = body.username;
		this.data[index].password = body.password;
		fs.writeFileSync("./data/data.json", JSON.stringify(this.data));
		successResponse(res, 201, this.data[index]);
	};
	// Delete user
	deleteUser = (req, res) => {
		let index = req.params.index;
		this.data.splice(index, 1);
		fs.writeFileSync("./data/data.json", JSON.stringify(this.data));
		successResponse(res, 201, {total: this.data.length});
	};
}

module.exports = User;
