const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const jsonParses = bodyParser.json();
const User = require("./controller/user.js");
const user = new User();
const port = 3000;

// load static files
app.use(express.static(path.join(__dirname, "public")));

// set view engine
app.set("view engine", "ejs");

// GET HOME PAGE
app.get("/home", user.home);
// GET GAME PAGE
app.get("/game", user.game);
// get data user
app.get("/user", user.getUser);
// create new user
app.post("/user", jsonParses, user.createUser);
// update user
app.put("/user/:index", jsonParses, user.updateUser);
// delete user
app.delete("/user/:index", user.deleteUser);

// listening
app.listen(port, () => {
	console.log("listening at this " + port + " port");
});
